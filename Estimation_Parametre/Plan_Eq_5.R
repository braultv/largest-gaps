### Estimation dist(theta_est,theta)
## Update 21/02/2023 vincent.brault@univ-grneoble-alpes.fr

setwd("/home/vbrault/LG/Estimation_Parametre")

rm(list=ls())

### Parameter plan
eps=0.05
Prop="Eq"

source("Plan.R")

